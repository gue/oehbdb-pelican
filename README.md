
# OEHBDB Pelican

Website generator for oehbdb data.
Based on [Pelican static site generator](https://getpelican.com/) and [oehbdb](https://codeberg.org/gue/oehbdb).

https://codeberg.org/gue/oehbdb-pelican


