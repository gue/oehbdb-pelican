AUTHOR = 'Günter'
SITENAME = 'ÖHB Daten'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Vienna'

DEFAULT_LANG = 'de'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = (('Handball Austria', 'https://www.oehb.at/'),
         ('nuLiga', 'https://oehb-handball.liga.nu/'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

# LOAD_CONTENT_CACHE = False

PLUGIN_PATHS = ["plugins"]
PLUGINS = [
    'scrapliga'
]


THEME = "themes/oehbd"


SCRAPLIGA_FEEDS_DIR = "../scrapliga/feeds"

PAGE_URL = 'p/{slug}/'
PAGE_SAVE_AS = 'p/{slug}/index.html'
