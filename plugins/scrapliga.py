import json
import logging
import datetime
from pelican import signals
from pelican.contents import Article, Page
from pelican.readers import BaseReader
from slugify import slugify

log = logging.getLogger(__name__)

groups = []
clubs = []
teams = []
courts = []
matches = []

def read_scrapliga_feeds(generator):
    global groups, clubs, teams, courts, matches

    base_dir = generator.settings.get('SCRAPLIGA_FEEDS_DIR').rstrip('/')

    with open("%s/groups.json" % base_dir, "r") as f:
        groups = json.load(f)

    with open("%s/clubs.json" % base_dir, "r") as f:
        clubs = json.load(f)

    with open("%s/teams.json" % base_dir, "r") as f:
        teams = json.load(f)

    with open("%s/courts.json" % base_dir, "r") as f:
        courts = json.load(f)

    with open("%s/matches.json" % base_dir, "r") as f:
        matches = json.load(f)


def get_club_teams(club):
    club_teams = []
    for team in teams:
        if team.get('club_name') == club.get('name'):
            club_teams.append(team)
    return club_teams


def addArticle(articleGenerator):
    settings = articleGenerator.settings

    # Author, category, and tags are objects, not strings, so they need to
    # be handled using BaseReader's process_metadata() function.
    baseReader = BaseReader(settings)

    for group in groups:
        name = group.get('name')
        contents = "Eine lustige gruppe %s" % name
        group_article = Article(contents, {
            'title': name,
            "date": datetime.datetime.now(),
            'category': baseReader.process_metadata("category", "Liga"),
            "tags": baseReader.process_metadata("tags", "tagA, tagB")
        })
        articleGenerator.articles.insert(0, group_article)


def add_pages(generator):
    settings = generator.settings

    # Author, category, and tags are objects, not strings, so they need to
    # be handled using BaseReader's process_metadata() function.
    baseReader = BaseReader(settings)

    team_pages = {}
    for team in teams:
        name = team.get('name')
        contents = "Team default content"
        team_page = Page(contents, {
            'title': name,
            "date": datetime.datetime.now(),
            'status': 'hidden',
            'slug': 'club/%s/team/%s-%s' % (slugify(team.get('club_name')), slugify(name), team.get('nuliga_id')),
            'category': baseReader.process_metadata("category", "Team"),
            'template': 'page',
            'team': team,
        })
        team_pages[name] = team_page
        generator.pages.append(team_page)

    club_pages = {}
    club_page_links = []
    for club in clubs:
        name = club.get('name')
        contents = "Ein lustiger Verein %s" % name
        club_teams = get_club_teams(club)
        team_page_links = []
        for team in club_teams:
            team_page = team_pages.get(team.get('name'))
            team_page_links.append((team.get('name'), team_page.url))
        team_page_links.sort(key=lambda v: v[0])  # sort alphabetically
        club_page = Page(contents, {
            'title': name,
            "date": datetime.datetime.now(),
            'status': 'hidden',
            'slug': 'verein/%s' % slugify(name),
            'category': baseReader.process_metadata("category", "Verein"),
            'template': 'club',
            'club': club,
            'team_pages': team_page_links
        })
        club_pages[name] = club_page
        club_page_links.append((name, club_page.url))
        generator.pages.append(club_page)

    club_page_links.sort(key=lambda v: v[0])  #  sort alphabetically

    club_index_page = Page('', {
        'title': 'Vereine',
        "date": datetime.datetime.now(),
        'status': 'published',
        'slug': 'vereine',
        'template': 'clubs',
        'club_pages': club_page_links

    })
    generator.pages.append(club_index_page)


    court_pages = {}
    court_page_links = []
    for court in courts:
        name = court.get('name')
        short_name = court.get('short_name')
        addr = "%s<br>\n%s, %s" % (court.get('address'), court.get('postal_code'), court.get('city'))
        court_page = Page(None, {
            'title': name,
            "date": datetime.datetime.now(),
            'status': 'hidden',
            'slug': 'halle/%s' % slugify(name),
            'category': baseReader.process_metadata("category", "Halle"),
            'template': 'court',
            'court': court,
            'address': addr
        })
        court_pages[name] = court_page
        court_page_links.append((name, court.get('postal_code'), court_page.url))
        generator.pages.append(court_page)

    court_page_links.sort(key=lambda v: "%s-%s" % (v[1], v[0]))  # sort alphabetically

    court_index_page = Page('', {
        'title': 'Hallen',
        "date": datetime.datetime.now(),
        'status': 'published',
        'slug': 'hallen',
        'template': 'courts',
        'court_pages': court_page_links
    })
    generator.pages.append(court_index_page)


def register():
    signals.generator_init.connect(read_scrapliga_feeds)
    signals.article_generator_pretaxonomy.connect(addArticle)
    signals.page_generator_finalized.connect(add_pages)
